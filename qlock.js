import './qlock.scss'

const QLOCK = {
  it: [0, 1],
  is: [3, 4],
  am: [7, 8],
  pm: [9, 10],
  quarter: [13, 19],
  twenty: [22, 27],
  fivee: [28, 31],
  twentyfive: [22, 31],
  half: [33, 36],
  tenn: [38, 40],
  to: [42, 43],
  past: [44, 47],
  nine: [51, 54],
  one: [55, 57],
  six: [58, 60],
  three: [61, 65],
  four: [66, 69],
  five: [70, 73],
  two: [74, 76],
  eight: [77, 81],
  eleven: [82, 87],
  seven: [88, 92],
  twelve: [93, 98],
  ten: [99, 101],
  oclock: [104, 109]
}

let oldMM
let isQlockLoaded = false

function getQlock () {
  const NOW = new Date()
  let { HH, MM, SS } = { HH: NOW.getHours(), MM: NOW.getMinutes(), SS: NOW.getSeconds() }
  if (oldMM === MM) return { activeList: [], currentMinute: MM }
  oldMM = MM
  if (MM >= 35) HH++

  const qlockList = [QLOCK.it, QLOCK.is, appendDeltaKey(MM), QLOCK[getHourKey(HH)]]

  if (qlockList.includes(QLOCK.oclock)) {
    qlockList.push(HH < 12 ? QLOCK.am : QLOCK.pm)
  } else {
    if (MM < 35) { qlockList.push(QLOCK.past) } else { qlockList.push(QLOCK.to) }
  }

  return { activeList: qlockList, currentMinute: MM, currentSec: SS }
}

function getHourKey (HH) {
  const HOUR = [
    'twelve',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve'
  ]
  return HH < 12 ? HOUR[HH] : HOUR[HH - 12]
}

function appendDeltaKey (MM) {
  const delta = parseInt(MM / 5)
  switch (delta) {
    case 0:
      return QLOCK.oclock
    case 1:
    case 11:
      return QLOCK.fivee
    case 2:
    case 10:
      return QLOCK.tenn
    case 3:
    case 9:
      return QLOCK.quarter
    case 4:
    case 8:
      return QLOCK.twenty
    case 5:
    case 7:
      return QLOCK.twentyfive
    case 6:
      return QLOCK.half
    default:
      break
  }
}

function updateClock () {
  let { activeList, currentMinute, currentSec } = getQlock()
  if (!activeList.length) return

  const qlockNodes = document.querySelectorAll('.qlock-text')
  for (let i = 0; i < qlockNodes.length; i++) {
    if (isActive(activeList, i)) addClass(qlockNodes[i])
    else removeClass(qlockNodes[i])
  }

  const minuteNodes = document.querySelectorAll('.qlock-minutes')
  currentMinute = currentMinute % 5
  for (let idx = 0; idx < minuteNodes.length; idx++) {
    minuteNodes[idx].style.removeProperty('animation-delay')

    if (currentMinute === 0) removeClass(minuteNodes[idx], 'reverse-animating')

    if (currentMinute === idx) {
      addClass(minuteNodes[idx], 'animating')
      if (!isQlockLoaded) minuteNodes[idx].style.setProperty('animation-delay', `${-1 * currentSec}s`)
    } else if (currentMinute === 4) {
      removeClass(minuteNodes[idx])
      removeClass(minuteNodes[idx], 'animating')
      addClass(minuteNodes[idx], 'reverse-animating')
    } else if (idx < currentMinute) {
      removeClass(minuteNodes[idx], 'animating')
      addClass(minuteNodes[idx])
    }
  }
  isQlockLoaded = true
}

function isActive (arr, idx) {
  let x = 0
  arr.forEach(i => {
    if ((idx > i[0] && idx < i[1]) || idx === i[0] || idx === i[1]) x = 1
  })
  return x
}

function addClass (el, classValue = 'active') {
  return el.classList.add(classValue)
}

function removeClass (el, classValue = 'active') {
  return el.classList.remove(classValue)
}

function replaceClass (el, oldClassValue, newClassValue) {
  return el.classList.replace(oldClassValue, newClassValue)
}

function setMinuteTrackScale () {
  let x = getComputedStyle(document.getElementById('qlock-minutes-hidden')).getPropertyValue('width')
  x = parseInt(x.replace('px', '')) / 4
  document.documentElement.style.setProperty('--minute-track-scale', x)
}

function setVisibilityListener () {
  var hidden, visibilityChange
  if (typeof document.hidden !== 'undefined') {
    hidden = 'hidden'
    visibilityChange = 'visibilitychange'
  } else if (typeof document.msHidden !== 'undefined') {
    hidden = 'msHidden'
    visibilityChange = 'msvisibilitychange'
  } else if (typeof document.webkitHidden !== 'undefined') {
    hidden = 'webkitHidden'
    visibilityChange = 'webkitvisibilitychange'
  }

  document.addEventListener(visibilityChange, () => handleVisibilityChange(hidden), false)
}

function handleVisibilityChange (hidden) {
  if (document[hidden]) {
    console.log('hidden')
  } else {
    console.log('shown')
  }
}

function onLoad () {
  const QLOCKstring = 'itlisasampmacquarterdctwentyfivexhalfstenftopasterunineonesixthreefourfivetwoeightelevenseventwelvetenseoclock'
  const qlock = document.getElementById('qlock')

  for (let idx = 0; idx < QLOCKstring.length; idx++) {
    const el = document.createElement('div')
    el.appendChild(document.createTextNode(QLOCKstring.charAt(idx)))
    el.setAttribute('class', 'qlock-text')
    qlock.appendChild(el)
  }
  updateClock()
  // setTimeout(setMinuteTrackScale, 100)
  setInterval(updateClock, 1000)
  setVisibilityListener()
}

onLoad()
