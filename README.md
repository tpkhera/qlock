# Qlock

An alphabetical clock inspired by [QLOCKTWO](https://qlocktwo.com/en/)

<img src="qlock.png" width="512" />

## Live Demo

https://thecalm.dev/qlock

### Prerequisites

```bash
yarn or npm
```

### Development

```bash
yarn               # installs the project dependencies
yarn start         # serves the project at localhost:8000
yarn run build     # builds the project to dist directory
```

